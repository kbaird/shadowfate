#!/usr/bin/env ruby
# frozen_string_literal: true

# random_noir_story.rb

# From _Deadlands: Noir_, pp74-79
class NoirStory
  BETRAYALS = [
    "Oops: Their employer spills the beans by accident. Granted, it's an innocent
    mistake, but that probably doesn't matter when the heroes find themselves on
    the business end of a Tommy gun!",
    "Revenge: Whether for an actual slight, some fallout from a previous investigation,
    or purely imagination, the client has a beef with the investigators and sets them
    up from the start.",
    "Save My Own Hide: The client gets in over his head somehow and gives up the
    heroes to save his own skin.",
    "Benevolent Betrayal: Out of some misguided notion or just plain naivete,
    their employer gives up the goods on the shamuses because she thinks it's
    actually for their own good."
  ].freeze

  DARK_SECRETS = [
    "Silent Partner: The client used to be involved in a business relationship
    with the target, possibly exactly the same crime as he's currently being
    investigated for, but doesn't want anyone to know.",
    "Bad Blood: The client is related to the subject in a way he hasn't revealed
    to the investigators—possibly even closely. The client may or may not be
    aware of this relationship.",
    "Patsy: The client is actually working for a competitor of the villain (who
    may just be a fall guy). The bad guy may be bad in his own right, but the
    client couldn't care less.
    She's using the investigators as proxies in a bid to take down another contender.",
    "Guilty!: Their employer is actually the guilty party. He's using the heroes
    to either establish a plausible alibi for himself or just to help him find
    any damning evidence
    before the police can."
  ].freeze

  EVENTS = [
    "Theft: An object of value has been stolen from the client. It may be an
    heirloom, a valuable objet d'art, or a shipment of some sort. Theft may
    also be embezzlement, where money is siphoned off an account rather than
    simply a burglar stealing a rare painting.",
    "Blackmail: The client is being threatened with the revelation of information
    that could ruin her socially and even result in criminal charges unless she
    buys her blackmailer's silence.",
    "Extortion: This is usually a strong-arm bid for protection money or a
    \"piece of the action.\" It differs from blackmail in that there is typically
    an implicit threat of violence or at least property damage backing up the
    demand. It's also one of the most profitable lines of Black Hand business!",
    "Missing Person: Someone close to the client, possibly a relative, friend,
    or business associate, has gone missing under suspicious circumstances. For
    whatever reason, the client either doesn't want to involve the police or
    normal channels have failed. It can also be slightly less ominous - perhaps
    the heroes are trying to track down the heir to an inheritance who's not been
    seen in years. Draw a card from the Action Deck. On a black card or a Joker,
    there's been foul play of some sort; the Perpetrator is responsible. On a red
    card, the disappearance has a less nefarious bent and the Perpetrator instead
    is a vital contact the heroes must identify through their investigation to
    locate the missing person.",
    "Murder: The investigators are asked to look into a murder that either the
    police have shelved, can't solve, or can't be trusted with.",
    "Fraud: Some sort of deception has occurred, most often one involving a sizeable
    monetary amount. It may involve insurance, personal property, or even forgery -
    whether of a legal document or a high-value piece of art.",
    "Kidnapping: Someone, usually close to the client, has been kidnapped. The
    kidnappers may have made a demand for money or something less tangible. The
    client needs the heroes to find the victim, usually, for whatever reason,
    without involving the authorities.",
    "Arson: Where there's smoke there's fire. And sometimes where there's fire,
    there's also an arsonist. A building or other piece of property belonging to
    the patron has been burned up—and someone's responsible!",
    "Menace: The employer is in fear for her life - maybe there have been threats
    or even previous attempts. She might not even know why, but that doesn't make
    the danger any less.",
    "Vetting: In this case, the client wants the heroes to do a little digging on
    someone, possibly a potential employee, a political candidate, a possible suitor,
    or an heir to a sizable will."
  ].freeze

  HIDDEN_MOTIVES = [
    "Distraction: The client is using the heroes' investigation to distract the police, the Black Hand, or other group or individual's attention while he commits
    a crime of his own. Roll on the Event table (ignoring Bodyguard and Background Investigation).",
    "Snipe Hunt: The client puts the investigators on a case to keep them from catching on to her own questionable activities before it's too late. Here the heroes
    pose a threat to their employer's interests, even if they don't realize it at first.",
    "Bait & Switch: The client points the group at one incident hoping to draw them into a related affair. She wants the investigators poking around in the second
    event, but to appear completely innocent in putting the shamuses onto the trail. Roll a second time on the Event table to find out the other crime (ignoring results
    of Arson or Embezzlement).",
    "Spoiler: Their employer doesn't really care about solving the original crime. He's really interested in damaging the reputation of a prominent figure who may
    or may not be the real villain."
  ].freeze

  HOOKS = [
    "Business or Corporate: A company, small business, or large corporation of some sort hires the investigators. The problem could be directly related to
    the business itself or be a personal matter for one of the owners or board members.",
    "Existing Contact: This is usually a former client who's back for repeat business. Attorneys and bail bondsmen are the most common—but it could also be
    anyone for whom the shamuses have worked in the past. It might also be a friend or family member of one of the characters. The good news is it's not as likely
    the existing customer is looking to pull a fast one as a complete stranger might be. The bad news is an old client knows more about how the investigators work if
    he is looking for a fall guy!",
    "High Society: A high profile member of society or a politician who is trying to avoid publicity seeks the gumshoes' aid in addressing a problem. These types
    of clients are among the most sought-after, not just because of their deep pocketbooks, but also because they can provide a reference to others in the same strata.",
    "Referral: The hook is someone who's been pointed toward the gumshoes by a mutual friend, family member, former client, or someone else who has an
    existing relationship with the group. Usually, the referral can provide someone to vouch for her, but not always someone who the heroes trust!",
    "Stranger: A total stranger approaches the heroes based on reputation, advertising, or maybe even a favorable news story if the characters are high profile
    enough. Yeah, it's risky, but the color of the money in his wallet is exactly the same color as it is in a friend's, right?",
    "Happenstance: The shamuses somehow become involved either by chance or accident. Perhaps one of the investigators spots an eye-catching article in the
    Times-Picayune, or maybe one actually stumbles onto the crime itself. Sometimes it's too juicy a mystery to pass up, sometimes it plucks at the heart strings, and
    other times, it marks a place where a crafty hero can get his foot in the door on a lucrative case. Since there's no “client” in this case (at least not at first),
    roll twice on the Perpetrator table—once for the villain and once for the victim."
  ].freeze

  MOTIVES = [
    "Greed: Plain and simple, money's the answer. Well, it could be property, a unique item, or even a promotion, but you get the idea. There's a tangible profit to be had.",
    "Political Gain: The act results in some improvement in the villain's legal status. It might well be a step toward securing a political office for himself
    (or a lackey) or it may result in a fall from grace for an opponent. On a more subtle level, the crime might be intended to influence one single piece of legislation or
    bureaucratic transaction.",
    'Revenge: The antagonist has an axe to grind with the client and the crime is the grindstone.',
    "Love: One of the oldest reasons man has been doing harm to other men over the years, love and the pursuit thereof, remains a powerful motivator both
    in the shadows and the well-lit ballrooms of society's upper crust.",
    "Knowledge: The villain seeks some bit of information and the crime is merely a side effect of that search. Draw a card from the Action Deck. On a red
    card, the client has no knowledge of the information. On a black card, she does (whether she shares that with the heroes is another matter). On a Joker, she
    knows the information, but doesn't realize the bad guy is looking for it.",
    "Survival: The client's actions, knowledge, or very existence threatens the villain in some fashion. Roll 1d4 and consult the table below. Initially, the client
    does not reveal this information. Draw a card from the Action Deck: on a red card, he doesn't even know it himself; on a black card, he does, but doesn't see a
    link until the heroes uncover the villain (or at least her motive). On a Joker, he knows from the very beginning but keeps it secret because it reveals he's more than a
    little dirty himself."
  ].freeze

  PERPS = [
    'Relative: The villain is a known relative of the client, usually closer than comfort allows.',
    "Friend: The culprit turns out to be a close friend— often a confidante—of the investigator's employer.",
    'Business Associate: A coworker, partner, employer/​employee, or simply someone with whom they have business dealings is behind the trouble.',
    "Corrupt Official: There's corruption at every level in New Orleans, and the scoundrel who's causing the heroes' patron grief sits comfortably on one of
    them. The villain might be a politician, a bureaucrat, policeman, or the like, of authority ranging from a beat cop all the way up to a senator—all of whom can cause
    no end of trouble for a group of lowly shamuses.",
    "Criminal: The client is the victim of a lone criminal or small gang of thugs. However, just because they're not tied with one of the main underworld organizations
    doesn't mean they're not sufficiently devious and dangerous to pose a threat.",
    "Competitor: This might be a business competitor, a rival suitor, athletic contender, or other person who is vying with the client for a goal. While it might seem on
    the surface that the object of competition is the motive, life is seldom simple.",
    "Outsider: The investigators' employer apparently has been targeted by an complete stranger. This might just be bad luck, the client's high-profile lifestyle, or
    just because the poor sap was an easy mark. On the other hand, there might be more going on than meets the eye.",
    "Organized Crime: Bad news, the Black Hand or Red Sect has taken an interest in the characters' patron."
  ].freeze

  THREATS = [
    'Physical: The client poses an actual physical threat to the villain.',
    "Financial: The patron or his business dealings represent a substantial danger to the bad guy's monetary wealth.",
    "Social: The heroes' employer constitutes a risk to the villain's standing in society, or within her peer group at the very least.",
    'Legal: The client (or knowledge he has) puts the culprit in jeopardy of arrest or conviction. ' \
      "Alternately, a more sophisticated villain's legislative maneuvers may be thwarted by the client's knowledge or actions."
  ].freeze

  TWISTS = [
    "Betrayed!: The client stabs the investigators in the back somehow. Sometimes this is intentional, and sometimes it's just innocent misadventure.
    See the Betrayed! table below to get the details.",
    "Double Blind: The apparent villain is actually a front for another villain who's trying to keep her involvement a secret. The real bad guy is likely to take
    rather extreme measures to cover up her own actions, but on the other hand, often the patsy she's fingering is carrying an axe to grind of his own. Roll on the
    Perpetrator Table a second time to find the real heavy.",
    'Hidden Motive: The client has the heroes investigating the trouble on false pretenses. See the Hidden Motive Table.',
    'Dark Secret: The client, etc., is hiding some dark secret about her past or other aspect of her life. Roll on the Dark Secret table.',
    "Mistaken Identity: Someone believes the client, the subject, or even one of the investigators is another person. Of course, the person with whom the other
    is confused has their own set of problems. Draw one card from the Action Deck. On a red card, it's the client, while a black card refers to the villain. On a
    Joker, the person with the identity mix-up is one of the investigators. Now, roll on the Perpetrator table to figure out with whom the poor sap is getting confused.",
    "Tangled Mess: Roll twice more on this table, ignoring this result—unless you're feeling really evil"
  ].freeze

  def self.many(count)
    (1..count).map { new }
  end

  def inspect
    items.inject("\n") do |acc, pair|
      ky, vl = pair
      this = "#{ky}:\n  #{vl}\n\n"
      acc + this
    end
  end

  private

  def action_deck(card_count = 1)
    deck.shuffle.take(card_count).join(' ')
  end

  def deck
    values = (2..10).to_a + %w[J Q K A]
    suits = %w[C D H S]
    values.map do |value|
      suits.map { |suit| "#{value}#{suit}" }
    end + [%w[Black Joker], %w[Red Joker]]
  end

  def evidence
    [
      %w[Direct Circumstantial].sample,
      case rand(1..10)
      when (1..5) then 'Physical'
      when (6..8) then 'Testimonial'
      else; 'Documentary'
      end
    ].join(', ')
  end

  def perp_explanation
    "\n  A black card or a Joker means the villain is somehow tied up in the supernatural, while a red card says the bad guy is 100 percent naturally evil."
  end

  def initialize
    @hook = HOOKS.sample
    @event = EVENTS.sample
    @perp = perp
    @motive = motive
    @evidence = evidence
    @twist = twist
    @action_deck = action_deck
  end

  def items
    { hook: @hook, event: @event, perp: @perp + perp_explanation, motive: @motive, evidence: @evidence, twist: @twist,
      action_deck: @action_deck }
  end

  def motive
    motive = MOTIVES.sample
    return [motive, THREATS.sample].join("\n  ") if motive.include?('Survival')

    motive
  end

  def perp
    perp = PERPS.sample
    return [perp, PERPS.sample].join("\n  ") if @hook.include?('Happenstance')

    perp
  end

  def twist
    twist = TWISTS.sample
    @perp = [@perp, PERPS.sample].join("\n  ") if twist.include?('Double Blind')
    @perp = [@perp, PERPS.sample].join("\n  ") if twist.include?('Mistaken Identity')
    return [twist, BETRAYALS.sample].join("\n  ") if twist.include?('Betrayed!')
    return [twist, HIDDEN_MOTIVES.sample].join("\n  ") if twist.include?('Hidden Motive')
    return [twist, DARK_SECRETS.sample].join("\n  ") if twist.include?('Dark Secret')

    twist
  end
end

count = (ARGV[0] || 1).to_i
puts NoirStory.many(count).inspect
