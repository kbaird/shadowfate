# Seattle 2072
- Foreign relations (pg145)
- Local corporations (pg166)
- Underworld (pg174)
- Other groups (pg150)

## Scenario ideas
- **West Point** digs (pg44)
- **Stoddard Security** (pg49)
- **Gray Line** Robbery of 2049 (pg52)
- **Lone Star** selling old arsenal (pg54)
- **Renton Mall** gang problems (pg60)
- **Bowman Metal Works** polluting (pg99)
  - PC Focus: **Jay**
- Harvest-time security for **Petrowski Farms** (pg129)
- Assassination attempts vs. Puyallup Mayor **Lon Campa** (pg143)
  - Although we may be earlier in the timeline
- **Evo** is anti-**People of the Book** [pg154; _Runner Havens_ pg70]
  **Horizon** might support them in order to hurt **Evo**

## NPCs
- **Harrison Kellerman**, crotchety talismonger (pg53)
- **Mama Pani**, talismonger (pg101)
