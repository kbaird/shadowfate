# Shadowfate

## [House Rules](House-Rules.md) for Magic

Magicians should take an [Extra](https://fate-srd.com/fate-core/extras) (technically implemented as a
[Gadget](https://fate-srd.com/fate-system-toolkit/gadgets-and-gear)) called (wait for it...)
**Magician** (or some type of **Adept**) that should specify the
[Tradition](http://adragon202.no-ip.org/Shadowrun/index.php/SR5:Magic:Traditions)
(Hermetic, Shamanic, Norse Ásatrú, Shintō, Voudoun, etc.). It should clarify what Attribute is your
primary magical stat, and provides a narrative excuse for you to use that stat to do magic stuff,
like casting spells and summoning spirits as lengthy ritual operations, or buffing up your Kung Fu
before or during a fight (as matches your concept). If that's all it does, it costs 0 Refresh.

Traditionally, _Shadowrun_ Magicians can
[Astrally Perceive and Project](https://shadowrun.fandom.com/wiki/Astral_Space). That would be a
[benefit](https://fate-srd.com/fate-system-toolkit/gadgets-and-gear#stunts) that costs 1 Refresh.

In addition, you can take Stunts called **Quick Spellcaster** and **Quick Summoner** (or alternate
Summoning options below) that let you cast spells or summon spirits (respectively) at-will.
> My thought here is that I want to give a lot of leeway for Magicians to do cool stuff with their
> power, but don't want them to overpower non-magical PCs. So these costs seem fair. Cyberware being
> more Fate Point-oriented also justifies mundane PCs having a higher Refresh.

In the base _Shadowrun_ rules, magic is typically powered by **Drain**, which can cause mental or
physical damage (very similar to Fate's stress, actually). I would allow a magician to voluntarily
take [Stress](https://fate-srd.com/fate-core/resolving-attacks#stress) or a
[Consequence](https://fate-srd.com/fate-core/resolving-attacks#consequences) to push themselves and
boost a roll pertaining to magic use. (NPCs can do the same.)

Magicians can also optionally choose a [Totem](https://shadowrun.fandom.com/wiki/Totem) /
[Mentor Spirit](http://adragon202.no-ip.org/Shadowrun/index.php/SR5:Mentor_Spirits_List). This can
be fleshed out in an Aspect, [Stunt](https://fate-srd.com/stuntmaker/),
[Extra](https://fate-srd.com/fate-core/extras) or whatever.

I figure _Buffy_ / _Angel_-style rituals can just be
[Challenges](https://fate-srd.com/fate-core/challenges), maybe with mandatory MacGuffins acquired
from quests, etc.

<details>
<summary>Summoning / Conjuring Elementals & Spirits</summary>

I see three potential options. Their differences could be a nice way to differentiate magical
[Traditions](http://adragon202.no-ip.org/Shadowrun/index.php/SR5:Magic:Traditions) between PCs.

### [Basic Creature Summoning](https://fate-srd.com/venture-city/creature-summoning)

> From [Venture City](https://fate-srd.com/venture-city)

The summoned Spirit's single **Good / +3** Skill is a decent representation of _Shadowrun_ Spirits
having powers useful for non-combat tasks. It's also simple. The PC doing this can just have another
character sheet ready-to-go for their spirit.

> This may be pretty powerful compared to other PCs. Maybe require an
> [Overcome](https://fate-srd.com/fate-core/four-actions#overcome) against the quality of the
> Spirit (+3) for successful summoning. This will work most times, but could force a
> Success at Cost or Drain every once in a while.

### [Storm Summoners](https://fate-srd.com/fate-system-toolkit/storm-summoners)

> From [Fate System Toolkit](https://fate-srd.com/fate-system-toolkit) (using the **Attribute**
> chosen for the **Magician** Extra rather than the **Conjuration** Skill).

The finer gradation of power levels could appeal more to somebody.

### 天下 Sorcery

[Tianxia: Spirits, Beasts, and Spells](https://drive.google.com/file/d/1Znh7VdXtmVPxwL1Wivb21GiB-g5kcL8f/view?usp=sharing)
details **Sorcery** (starting on page 29) and specifically **Exorcists** (on page 46). These options deviate from traditional
_Shadowrun_ magic a fair bit, but that's fine.

### Common Notes

For [Watcher Spirits](https://shadowrun.fandom.com/wiki/Watcher_Spirit), I think we could just
handle that as a [Create an Advantage](https://fate-srd.com/fate-core/four-actions#create-an-advantage)
action.

If someone wants to play a Rigger, we could use similar rules to handle swarms of drones, etc.
</details>
