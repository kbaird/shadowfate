# PCs

## Questions for everyone
- Do you already have a clear enough conception for a **High Concept**?
- SINner or SINless?
  - National SIN or megacorporate SIN?
- From Seattle? (if we're centered there)
- Family, friends?
- Do the PCs know each other?
- Is there anything you won't do for pay?
- Is there anyone you won't work for?

### From SR6, pg60
- Where were you born?
- Where were you raised?
- Where are you now (literally and metaphorically)?
- How does your character feel about the darker aspects of the world?

### Fleshing Out the Character, SR2e pg47

Ask if anyone wants a **Trouble** Aspect suggesting that their PC is of specific
interest to a powerful entity: corporation, demon, corporate demon, demonic
corporation, etc.

In which [district](SR4e_metroplex_map.png) do the PCs live? Does anyone live with anyone else?

<details>
<summary>Brian</summary>

- Ork Rigger originally from CFS
- SINless; Orphan
- Cyber(sw)arm from being experimented on in an **Evo** black site

Questions posed in Discord:
- How old is he? How many years ago was he captured, escaped, brought into **ORC**, left ORC, respectively?
  - Note to self: **Crashcart** (founded 2052) does this already officially [3E _Corporate Download_, pg116].
- San Francisco and knowledge of Japanese would be an easy fit for pre-**Evo** **Yamatetsu**.

> Redding was the destination for many metahumans relocated from the
> **California Protectorate**, and still has a very high percentage of them.
- _Sixth World Almanac_, pg177

Timeline of CFS & Evo
- 2032: Yamatetsu founded
- 2036: CFS kicked out of UCAS. TT takes land in the north, Aztlan takes San Diego.
- 2037-02-07: Japan takes SF.
- 2042: Yamatetsu becomes a AAA
- 2050: Tadamako Shibanokuji wins back Chairman position in Yamatetsu, makes it more metahuman-friendly.
        Buttercup reveals her true Free Spirit nature.
- 2052: Crashcart founded. They run free clinics for SINLess, but are rumored to test cyberware on patients.
        Presumably, Brian's PC was one of those test subjects.
- 2059-01-07: Tadamako Shibanokuji has a stroke. His voting rights go to Saru Iwano & Yamatetsu gets more
              anti-metahuman again. Yuri Shibanokuji inherits his father's shares, becomes the new
              chairman, and moves the company to Vladivostok after an assassination attempt.
- 2061-10-27: Big quake in Bay Area
- 2061-10-29: Japanese troops ordered to withdraw from SF
- 2061-11: Colonel Keiji Saito disobeys orders and siezes SF.
           He also expands control past Sacramento and Fresno.
- 2068 (late): Colonel Saito kicked out of SF by CFS (backed by Ares).
- 2070: Campaign start

</details>

<details>
<summary>Chris</summary>

- "Lindsey McDonald" corporate drone on the inside
  - Ask him what kind of corp.
    - AAA? If so, corporate or national citizen?
    - Maybe cool stuff with Horizon & Consensus if that appeals to him.
      - Horizon info starting pg37 of _Corporate Enclaves_
        - **Pathfinder Multimedia**, movies, [music](Plots/Music-Business.md), etc.
        - **Charisma Associates**, the "single most successful PR firm in the
          world, and possibly in all of human history"
        - **Singularity**, programming studio
- Suggest Stunts from _Shadow of the Century_, especially **Face**

I like the idea of having the PC's boss be a white Human woman named "Deb".

</details>

<details>
<summary>Jay Novikov</summary>

- "Grumpy Jeeves" martial artist / physical adept
- Wants to do heists

</details>

<details>
<summary>Sophie Sullivan</summary>

- Female Elf with European ancestry
- Former corporate assassin, needs to return to it due to financial stress
- 4th World Elven parents are disappointed in her for not being "Elfy" enough.
- Born in **Tír Tairngire**, but moved to **Tarislar** around age 10.
  - Dual citizen with **Horizon**.

Questions
- What nation(s) have the parents lived in / aligned with throughout history?

Timeline of **Tír Tairnire** & **Tarlislar**
- 8238BCE: Beginning of the [Fourth World](https://shadowrun.fandom.com/wiki/Fourth_World)
- ?BCE: Sophie's parents born sometime in the Fourth World
- 3113BCE-08-12: **Thera** (Atlantis) sinks. End of the Fourth World.
- ?: Sophie's parents do things for ~5k years
- 2018CE: **Treaty of Denver** forms the SSC (among others)
- 2029: SSC opens its borders to all metahumans
- 2035-03-11: **Lugh Surehand** announces TT's secession from the SSC.
- 2036: CFS kicked out of UCAS. TT takes land in the north, Aztlan takes San Diego.
- 2039-02-07: **Night of Rage**. Seattle Elves go south toward TT, but end up founding
  **Tarislar**.
- 2042: **Sophie** born
- 2052: Sophie & parents move to **Tarislar**.
- 2053: Tír forces attack northern CFS
- 2058: **Rinelle ke'Tesrae** ("Rebels of the Spire") makes its first appearance
- 2065-01-05: **Larry Zincan** wins first free election as **High Prince**
- 2070: Campaign start

</details>

<details>
<summary>Thomas Jones</summary>

Engineered "Prototype Battle mage" from a separatist community on a repurposed cruise ship.
Spellcaster. "Human" with transgenic additions. Liberal Indonesian-flavored muslim (presumably
Sunni).

Asked Paul about [JIL](https://en.wikipedia.org/wiki/Jaringan_Islam_Liberal)

</details>
