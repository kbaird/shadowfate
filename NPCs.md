# NPCs

## NPC
## NPC
## NPC
## NPC
## NPC
## NPC
## NPC
## NPC
## NPC
## NPC
## NPC

## Deb Taylor
- The PCs' boss at [Horizon](Horizon.md)

## Suren ("Benji") Binyamin
- Born **Soren Jaime Benjamin-Zucco**
- Officer in the Puyallup Rangers (new cops)
- Italian/Iranian. Elf.

## James ("Jimmy") Mitchell Kincaid
- Burned-out mage in north ("downtown") Puyallup. White (Irish descent?) Elf. Raised Catholic.

## Dexter Pinkerton
- Dwarf of African descent in Downtown. Private Investigator. Mundane.

## Ḥazqiyāl ("Zeke") Qasīm / ﺡﺰﻘﻳﺎﻟ ﻖﺴﯿﻣ
- Egyptian-Seattlite Ork Magician
- https://static.wikia.nocookie.net/shadowrun_wikickstart/images/5/5b/Pc_orkmale_04_slick.png
- Knows **Jay Novikov**. Maybe they live in the same apartment building in Auburn.

## The Sullivans
- Live in a walled compound in **Tarislar** that contains their centuries-old
  home transported from Europe.

## Anthony ("Tony") Visconti
- Italian/Irish Leprechaun in the Bay Area. Worked with **Brian's PC** in the
  **Metahuman People's Army** [3E _Shadows of North America_, pg51]
