# The Music Business

[_Attitude_ pg49]

- **Anhedonia**, **Christy Daee**, **Darkvine**, and **Deirdre** are from **Tír Tairngire**
- **Christy Daee**, **Deridre**, and **Maria Mercurial** have **Horizon** contracts
  - Probably **Anhedonia** as well
- **Barry Mana** and **Teiko Ikemoto** have **Mitsuhama** contracts
- **Crimetime** and **Orxanne** live in **CFS**
- **Barry Mana**, **Concrete Dreams**, **The Elementals**, **Grim Aurora**, **Latch-Key Kids**,
  **Maria Mercurial**, **Shield Wall**, and **Wild Cards** are from **Seattle**
- **The Tolson Twins** don't leave the **CAS**

See also [On the Run](Horizon/01-On-the-Run.md.xz)

## Sportball

- **Miko Nabuto** plays for the Lakers, owned by **Horizon** [_Attitude_ pg145]
