![Logo](https://iconape.com/wp-content/files/uc/247530/svg/247530.svg)

# Shadowfate

A campaign using the rules from
[_Fate Freeport Companion_](https://drive.google.com/file/d/1XaxpXg_wv2nXdOwmrO0F07hMJrh2x-rL/view?usp=drive_link)
(more or less, see [House Rules](House-Rules.md)), set in
[_Shadowrun_](https://drive.google.com/drive/folders/16tXd5HLoweQH0kexT379lietYhPDRClV?usp=share_link)'s
Sixth World, probably ~2070 CE.

> (Note to self:
> consider [Campaign Starts](https://www.youtube.com/watch?v=KyKHGBVBAgw),
> especially **Funeral** or the dream version of **Alternate Future**.)

I think it would be good to have each player create 2-3 [NPCs](NPCs.md). Work
with them to figure out a setting-appropriate version of what they're getting at
(maybe a connection to an existing meta-narrative NPC). That can help determine
what the Current and Impending Issues can be.

<details>
<summary>Campaign Aspects</summary>

> I guess officially called
> [Game Aspects](https://fate-srd.com/fate-core/types-aspects#game-aspects).
> These and the Issues below can be subject to
> [World Advancement](https://fate-srd.com/fate-core/world-advancement).

_Shadow of the Century_ calls this a **Series Issue** (for TV) or **Big Issue** (for a movie).
- **The Belly of the Beast**?
- **Cyberpunk / Fantasy Mashup** AKA **The Sixth World**?
- **Cyberpunk with a Chance of Rain**?
- **Everything is Hackable**?
- **Evil is Everywhere**? / **Evil Doesn't Always Win**
- **Heroes for Acrimonious Anarchy**
  - Ask Chris what this means in more detail
- **Lesser of Two Evils**?
- **Life is Cheap**?
- **A Little Moral Compromise Never Hurt Anyone**?
- **Misfits United**?
- **Multifaceted Cold War** / **Wilderness of Mirrors**?
- **Neo-Noir**?
- **Power Corrupts**?
- **A Pretty Face Can Hide Anything**?
- **Shadowrunners for Hire**?
  - This is basically the stock / bog-standard campaign assumption for most _Shadowrun_ games.
    I find it a little edgelordy and tiresome, but there's a ton of easily re-usable material.
- **The Only Good Guys in a Crapsack World**?
  - This is kind of directly opposed to the above, but trying to balance both could be quite
    the tightrope, if that's what people want.
- **Trust No One**?
- **We Work for Horizon** (if true).
  - Mo suggested this as a Campaign Aspect to deal with funding, etc., rather than
    [Character Aspects](https://fate-srd.com/fate-core/types-aspects#character-aspects) or
    [Wealth Stress](https://fate-srd.com/fate-system-toolkit/wealth).
- **What Is My Purpose**?

> The [Tianxia PDF](https://1drv.ms/f/s!AtTqgSduvAo9myRN5GLYs3_IFE6P?e=PPmidM) has some good
> discussion of Campaign Aspects and how they relate to genre.

</details>

<details>
<summary>Current / Impending Issues</summary>

_Shadow of the Century_ calls this a **Season Issue** (for TV) or **Subplot Issue** (for a movie).

Sample issues to choose from, modify, or completely ignore:
- **Build/Protect the Neighborhood**
- **Crossing the Bridge I Tried to Burn** (from Phil)
- **Get Revenge on ____** or **Protect ____**
- **Independence for Seattle!** or **UCASian Loyalism Forever!**
- **Mysterious Coup**
- **Pawns for Immortals**
- **Personal Growth** (joining a magical group or hacker collective, acquiring some MacGuffin, etc.)
- **Solve the Mystery of ____**
- **Take Horizon Down From the Inside**
- **Tensions with the Sovereign Tribal Council**
- ~~**Horrors from Beyond**~~ (Mo is pretty uninterested in Cosmic Horror, so maybe not this.)
- ~~**Mob War** (**Boromar** vs. **Daask** from _Sharn Noir_)~~ (Mo is fairly uninterested in this, as well.)

</details>

<details>
<summary>Resources</summary>

  <details>
  <summary>Rules PDFs</summary>

- [_Shadowrun_ 4th ed](https://drive.google.com/drive/folders/16tXd5HLoweQH0kexT379lietYhPDRClV?usp=share_link)
  - This will be useful for all sorts of inspiration, examples of gear / cyberware, descriptions of how magic works, etc.
  - [Street Magic](https://1drv.ms/b/s!AtTqgSduvAo9o0qT-Pv2tUDfh0IQ?e=iokvIZ) goes into more detail about (surprise)
    magical topics, including traditions and Mentor Spirits.
- [_Fate Freeport Companion_](https://drive.google.com/file/d/1XaxpXg_wv2nXdOwmrO0F07hMJrh2x-rL/view?usp=drive_link)
  - I don't love the spellcasting in this book, and it's not a great fit for _Shadowrun_ anyway. See
  [House Rules](House-Rules.md) instead for magic (unless players feel otherwise).
- The [Tianxia PDF](https://1drv.ms/f/s!AtTqgSduvAo9myRN5GLYs3_IFE6P?e=PPmidM) is where Mo's martial arts come
  from, and is a good specific take on tailoring Fate rules to specific genre, even if that genre isn't what this
  campaign is likely to resemble.
  - However, [Tianxia: Spirits, Beasts, and Spells](https://drive.google.com/file/d/1Znh7VdXtmVPxwL1Wivb21GiB-g5kcL8f/view?usp=sharing)
    does have some good magical stuff, especially concerning exorcism, if anybody wants to play that kind of magician.

  </details>

  <details>
  <summary>Random Number Generators</summary>

- [Online Fate Dice Roller](https://matita.github.io/fatedice/)
- [**Deck of Fate** mobile app](https://deck-of-fate.hiddenachievement.com/)

  </details>

  <details>
  <summary>Google Maps</summary>

- Seattle, despite some names
  - [Seattle 2072 (Shadows of Seattle)](https://www.google.com/maps/d/edit?mid=1v4rEjVXWWFD3WbKUY8zw1ZJeDAZASMma&usp=sharing)
  - [Shadowrun - Districts, POI, Security Level, Gangs](https://www.google.com/maps/d/viewer?mid=1PtcszRc7G4afdH9jNYMd3R9naeo&hl=en_US&ll=47.40055357054477%2C-122.21105545703125&z=8)
  - [Shadowrun World Map Project 1.2](https://www.google.com/maps/d/viewer?mid=1_1ES7tdzvO65eeErMFvEu_uedVM&femb=1&ll=47.436229654261574%2C-122.68441737482755&z=9)
  - Plus [a video about the topographic history of the city](https://youtu.be/Iv1yr1zu0xQ)
- Shadowrun Political World Map
  - [2072-01-01](https://www.google.com/maps/d/u/0/viewer?mid=198dzJpUbwaNzfrQYOLkZW6uWRYBV0FLn&ll=18.263840774848628%2C-28.97893425312509&z=2)
  - [2079-12-20](https://www.google.com/maps/d/u/0/viewer?mid=1TNCj4CZg5SgOowBYfYgeyrmLuI3a_COO&ll=-3.81666561775622e-14%2C-28.97893425312509&z=1)
- [National Flags](https://imgur.com/a/shadowrun-flags-of-north-america-2072-ervgm)
  - I'm thinking the flag for CFS at the site is actually used by the **California Protectorate**
    (under Japanese control), while the still independent CFS retains the existing CA state flag.
- [Hippies around Mt. Shasta](https://www.youtube.com/watch?v=wE5qVpw7ktI)

  </details>
</details>

## [House Rules](House-Rules.md)
