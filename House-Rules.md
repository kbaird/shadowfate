# Shadowfate

## House Rules

By default,
[_Fate Freeport Companion_](https://drive.google.com/file/d/1XaxpXg_wv2nXdOwmrO0F07hMJrh2x-rL/view?usp=drive_link)
uses the 6 D&D Attributes in place of Skills with a (+3, +2, +2, +1, +1, +0) array.

> I would theoretically be open to the standard _Fate Accelerated_
> [Approaches](https://fate-srd.com/fate-accelerated/how-do-stuff-outcomes-actions-and-approaches#choose-your-approach),
> _Dresden Files Accelerated_'s **Flair**, **Focus**, **Force**, **Guile**, **Haste**, and **Intellect**, or
> _Fate of Cthulhu Accelerated_'s **Brutal**, **Bold**, **Cautious**, **Clever**, **Covert**, and **Fast**.

<details>
<summary>Advancement / Milestones</summary>

I think by far the best feature of _Cypher System_ is how it handles [Arcs](../PDFs/CS_Arcs.pdf).
I'd like to adapt that for this campaign to track
[Milestones](https://fate-srd.com/fate-accelerated/getting-better-doing-stuff-character-advancement#milestones).
</details>

<details>
<summary>Decking / Hacking / Netrunning</summary>

I'm inclined to say that a suitable Aspect will just allow use of an Attribute (most likely
Intelligence) for such activity. A PC can spend
[Refresh](https://fate-srd.com/fate-core/stunts-refresh#adjusting-refresh) on
[Stunts](https://fate-srd.com/stuntmaker/) or
[Extras](https://fate-srd.com/fate-core/extras) to give bonuses on top of that.

The [Tianxia PDF](https://1drv.ms/f/s!AtTqgSduvAo9myRN5GLYs3_IFE6P?e=PPmidM) has a chapter on
specific Kung Fu styles that could obviously be useful for anyone thinking of playing an
unarmed combatant, but I was thinking last night it could even be used more metaphorically for
someone's decking/netrunning style, if they want to get that specific.

> (Note to self: consider
> [Obstacles](https://fate-srd.com/fate-adversary-toolkit/types-adversaries#obstacles) for
> SR1-style color-coded Decking nodes.)
</details>

<details>
<summary>Gear and Cyberware / Augmentations</summary>

PCs can certainly use standard
[Weapons and Armor](https://fate-srd.com/fate-core/more-examples-extras#weapon-and-armor-ratings),
and [Gadgets](https://fate-srd.com/fate-system-toolkit/gadgets-and-gear). I also have the Fate
versions of _Interface Zero_ and _Mindjammer_, both of which have appropriate gear, and the
[4th Edition of _Shadowrun_](https://drive.google.com/drive/folders/16tXd5HLoweQH0kexT379lietYhPDRClV?usp=share_link)
can serve as inspiration. We can
[track ammo](https://fate-srd.com/fate-codex/lock-and-load-using-ammo-fate) if people want to.
(I kind of like the [Ammo Aspects](https://fate-srd.com/fate-codex/lock-and-load-using-ammo-fate#ammo-aspects)
option if we do.)

_Uprising_ has **Augmentations** on page 152.

We can also reskin the **Summoning / Conjuring** notes in the [Magic](House-Rules-Magic.md) section for drones.
</details>

[**Magic**](House-Rules-Magic.md)

<details>
<summary>Wealth / Moolah / Filthy Lucre</summary>

We tend to gloss over this stuff, so I figure the standard **Wealth** rules from
[_Fate Freeport Companion_](https://drive.google.com/file/d/1XaxpXg_wv2nXdOwmrO0F07hMJrh2x-rL/view?usp=drive_link),
pg52 (where this is all abstracted with character Aspects and Boosts) is reasonable. I'm open to
going crunchier (cyberpunk genre conventions might suggest doing so).

> I briefly considered **Credit Stress** from _Mindjammer_, but that's dependent on a _Resources_
> skill. People can just voluntarily take a
> [Consequence](https://fate-srd.com/fate-core/resolving-attacks#consequences) for
> [Success at a Cost](https://fate-srd.com/fate-core/what-do-during-play#succeed-at-a-cost) to
> achieve a similar enough result.

I would be open to [Wealth Stress](https://fate-srd.com/fate-system-toolkit/wealth) if we want
money to matter more in this campaign than others we've done in the past.

Mo suggests just having **We Work for Horizon** as a
[Campaign Aspect](https://fate-srd.com/fate-core/types-aspects#game-aspects) instead.
</details>

<details>
<summary>Miscellanea</summary>

- No Corruption Stress (Mo is pretty uninterested in Cosmic Horror)
- No hard requirement for the
  [Phase Trio](https://fate-srd.com/fate-core/phase-trio). Just pick
  [Character Aspects](https://fate-srd.com/fate-core/types-aspects).
- Base [Refresh](https://fate-srd.com/fate-core/stunts-refresh#adjusting-refresh) of 6 (not 3),
  but no [Stunts](https://fate-srd.com/fate-core/stunts-refresh) for
  free (as opposed to 3 free ones). This evens out. The idea here is two-fold:
  1. I think defining Cyberware and/or Spells as the
  [Function Aspects](https://fate-srd.com/fate-system-toolkit/gadgets-and-gear#functions-and-flaws)
  of [Gadgets](https://fate-srd.com/fate-system-toolkit/gadgets-and-gear) fits
  their "lots of these things, but they come up infrequently" status, and having
  more Fate Points available to Invoke those makes sense.
  2. Chris also doesn't love Fate Stunts, so you're not penalized for not taking any. But here's
  [some advice on how to make cool Stunts](https://fate-srd.com/fate-codex/stunts-are-cool).

</details>
