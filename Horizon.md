# Horizon Group

AAA Megacorp, extraterritorial, able to issue SINs
- CEO: Gary Cline
- World HQ: Los Angeles, PCC

Slogans:
- "We Know What You Think"
- "Moving Minds"

The newest face on the AAA scene, **Horizon** used the years following the Crash to
take advantage of its close relationships with both **Tír Tairngire** and the
**Pueblo Corporate Council** to secure itself a position on the **Corporate Court**.
Based in the midst of media wonderland Los Angeles, the corp has managed to score
many exclusive contracts for dealing with the development of California, and its
star is currently on the rise. With a charismatic ex-sim star at the helm, Horizon
is poised for great things at the dawn of the 2070s.

Horizon specializes in anything that can be used to manipulate opinion (personal
or public), including public relations, advertising, mimetics, viral marketing,
trendsetting, and social networking. Its corporate culture is much less hierarchical
than those of most other megas, emphasizing consensus, workgroups, trends, and
“people-centered” management models. Employees are well taken care of and encouraged
to develop their talents and pursue their interests on company time—though, of
course, Horizon expects to share in the fruits of their innovation.

In addition to its primary focus on entertainment and media pursuits, Horizon is
also strong in consumer goods and services, real estate and development, and
pharmaceuticals.

> Pop culture is another Horizon specialty. Between
> RockNet’s edutainment broadcasting and their line of A Whole
> New You™ clinics, the megacorp is quite literally shaping the
> minds and bodies of Seattle. Horizon is also a major provider of
> legal recreational drugs, competing with heavies like **Universal
> Omnitech** and **Shiawase Medical**.

> Horizon sponsors public schools in Seattle’s poorest districts,
> supplying them with the latest in edutech. Each kid gets a
> free commlink with a package of educational software and free
> subs to specialized Horizon media feeds. In return, Horizon
> gets to keep records of their social and spending habits. The
> kids have to deal with Horizon’s content filters and personally-
> tailored advertising/marketing schemes, but they get a far better
> education then they normally would. Not a bad deal, if you ask me.
- _Runner Havens_ pg77

## Horizon Principles

> During my new-employee orientation, I was introduced to the “Horizon Principles.” These are designed to guide employees
> in making daily decisions. All proposals for new research projects or creative endeavors are required to address how the new effort
> reflects these principles (I kid you not). The principles are:

> **Social Consciousness**: All new projects should reflect a commitment to conscientious environmental stewardship,
> improving the life of all metahumanity, and otherwise benefiting the global community.

> **Workplace Synergy**: All new projects should support employees by contributing to a collaborative, creative, supportive,
> positive workplace that emphasizes interpersonal communication and relationships.

> **Philanthropic Profitability**: All new projects should derive profits from business practices that help create a cycle of positive
> change by supporting environmental stewardship and social consciousness while fostering philanthropy.
- _Corporate Guide_ pg95

## Seattle Operations

Seattle HQ is at the **Horizon Creative Focus Retreat**,
1 Horizon Way, Renton [Seattle2072 pg93].
> In the interest of never-ending corporate oneupsmanship, Horizon decided to
> build their new “Creative Focus Retreat”
> [higher up on Cougar Mountain](https://maps.app.goo.gl/9oaoBbNj71AxvXKZ7) than
> the previous resort there or any other structure, including putting in a new road
> and VTOL landing pads. In essence, the place is a combination luxury resort and
> corporate work environment, where valued and successful employees can come
> and spend a “working vacation” enjoying the facilities and getting the creative
> juices flowing in whatever way works for them.

> So you’re likely to find “Horizoneers” engaged in pickup games of basketball
> on the quad or playing AR games as you are to see them in meetings. Hell, talking
> business while playing B-ball is Horizon’s idea of a “business meeting.” So in
> addition to the usual workstations, creative labs, and meeting rooms, the place
> has a spa, workout rooms, a pool, and its own restaurants that put the Cougar
> Mountain Resort to shame.

> Still, don’t let the open and relaxed atmosphere fool you; security at the
> retreat is tight. First off, access to the site is restricted, and all the approaches
> are monitored. There’s only one access road, and otherwise you’re climbing the
> mountain to the peak. RFID tags are required to be on site and, combined with
> passcodes, provide access to sensitive areas (both physically and in the Matrix).

## Politics

> **Josephine Dzhugashvili** (Independence Party Gubernatorial candidate in 2070)
> often cites the **Pueblo Corporate Council** as an economic role model, and she
> stands for increased relations with both the **NAN** and the Pacific Rim states.
> Interestingly enough, campaign financing records (which are completely public)
> show **Aztechnology**, **Horizon**, and **Evo** as her major contributors.
- _Runner Havens_ pg68
