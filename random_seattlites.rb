#!/usr/bin/env ruby
# frozen_string_literal: true

# random_seattlites.rb

# Follow demographics from Seattle 2072, pg8
class Citizen
  def self.many(count)
    (1..count).map { new }
  end

  def inspect
    [
      @ethnicity[0..1].to_s.capitalize(),
      @sex.to_s.upcase(),
      @metatype[0].to_s.capitalize()
    ].join()
  end

  private

  def d100
    rand(1..100)
  end

  def initialize
    @ethnicity = ethnicity
    @metatype = metatype
    @sex = sex
  end

  # This is from current Googling.
  # Probably too-low :native for the setting.
  def ethnicity
    case d100
    when (1..61) then :white
    when (62..78) then :asian
    when (79..84) then :black
    when (85..92) then :hispanic
    when (93..99) then :'multi-racial'
    when (100) then :native
    end
  end

  def metatype
    case d100
    when (1..66) then :human
    when (67..79) then :elf
    when (80..81) then :dwarf
    when (82..97) then :ork
    when (98..99) then :troll
    when (100) then :x
    end
  end

  def sex
    #%i[♀ ♂].sample
    %i[f m].sample
  end
end

count = (ARGV[0] || 10).to_i
puts Citizen.many(count).inspect
